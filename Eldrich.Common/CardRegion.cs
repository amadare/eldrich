namespace Eldrich.Common
{
    public class CardRegion
    {
        public int PaddingX { get; set; }
        public int PaddingY { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public string Name { get; set; }
    }
}