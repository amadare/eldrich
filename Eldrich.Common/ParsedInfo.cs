﻿namespace Eldrich.Common
{
    public class ParsedInfo
    {
        public string RegionName { get; set; }
        public string Value { get; set; }

        public ParsedInfo()
        {
        }

        public ParsedInfo(string regionName, string value)
        {
            RegionName = regionName;
            Value = value;
        }
    }
}