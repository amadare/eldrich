using System.Collections.Generic;

namespace Eldrich.Common
{
    public class ParsingLayout
    {
        public int LeftPadding { get; set; }
        public int TopPadding { get; set; }
        public int CardWidth { get; set; }
        public int CardHeight { get; set; }
        public int CardXCount { get; set; }
        public int CardYCount { get; set; }
        public string Pages { get; set; }
        public List<CardRegion> Regions { get; set; }
    }
}