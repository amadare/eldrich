using System.Collections.Generic;

namespace Eldrich.Common
{
    public class ParsedCard
    {
        public List<ParsedInfo> ParsedRegions { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public int Page { get; set; }

        public ParsedCard()
        {
            ParsedRegions = new List<ParsedInfo>();
        }
    }
}