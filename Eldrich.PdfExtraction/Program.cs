﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using Eldrich.Common;
using java.awt;
using org.apache.pdfbox.pdmodel;
using org.apache.pdfbox.util;

namespace Eldrich.PdfExtraction
{
    class Program
    {
        static void Main(string[] args)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(ParsingLayout));
            XmlSerializer cardsSerialiser = new XmlSerializer(typeof(List<ParsedCard>));

            if (args.Length == 0)
            {
                Console.WriteLine("args: file [config (default:config.xml)] [out (default: out.xml)]");
                return;
            }

            var file = args[0];
            var config = args.Length > 1 ? args[1] : "config.xml";
            var outFile = args.Length > 2 ? args[2] : "out.xml";

            ParsingLayout layout;
            using (var stream = File.Open(config, FileMode.Open))
            {
                layout = (ParsingLayout)serializer.Deserialize(stream);
            }
            
            PDDocument doc = PDDocument.load(file);

            List<ParsedCard> cards = new List<ParsedCard>();
            foreach (int page in IterateStringRange(layout.Pages))
            {
                Console.WriteLine($"page {page}");
                for (int i = 0; i < layout.CardYCount; i++)
                {
                    for (int j = 0; j < layout.CardXCount; j++)
                    {
                        Console.WriteLine($"\tcard #{j+i * layout.CardXCount}");

                        PDFTextStripperByArea stripper = new PDFTextStripperByArea();
                        foreach (var region in layout.Regions)
                        {
                            var x = layout.LeftPadding + layout.CardWidth * j + region.PaddingX;
                            var y = layout.TopPadding + layout.CardHeight * i + region.PaddingY;
                            var rect = new Rectangle(x, y, region.Width, region.Height);
                            stripper.addRegion(region.Name, rect);
                        }

                        stripper.extractRegions((PDPage)doc.getDocumentCatalog().getAllPages().get(page - 1));
                        var card = new ParsedCard { X = j, Y = i, Page = page };
                        foreach (var region in layout.Regions)
                        {
                            var text = stripper.getTextForRegion(region.Name);
                            card.ParsedRegions.Add(new ParsedInfo(region.Name, text));
                        }

                        cards.Add(card);
                    }
                }
            }
            using (var stream = File.Open(outFile, FileMode.Create))
            {
                cardsSerialiser.Serialize(stream, cards);
            }
            Console.WriteLine("done");
            Console.ReadKey();
        }

        public static IEnumerable<int> IterateStringRange(string range)
        {
            foreach (string s in range.Split(','))
            {
                // try and get the number
                int num;
                if (int.TryParse(s, out num))
                {
                    yield return num;
                    continue; // skip the rest
                }

                // otherwise we might have a range
                // split on the range delimiter
                string[] subs = s.Split('-');
                int start, end;

                // now see if we can parse a start and end
                if (subs.Length > 1 &&
                    int.TryParse(subs[0], out start) &&
                    int.TryParse(subs[1], out end) &&
                    end >= start)
                {
                    // create a range between the two values
                    int rangeLength = end - start + 1;
                    foreach (int i in Enumerable.Range(start, rangeLength))
                    {
                        yield return i;
                    }
                }
            }
        }
    }
}
