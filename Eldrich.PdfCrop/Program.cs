﻿using WebSupergoo.ABCpdf10;

namespace Eldrich.PdfCrop
{
    class Program
    {
        static void Main(string[] args)
        {
            var doc = new Doc();
            doc.Read(@"F:\eldrich\Eldritch.Horror.PnP\Активы.pdf");

            doc.Rendering.DotsPerInch = 300;

            for (int i = 1; i <= 2; i++)
            {
                doc.PageNumber = i;
                doc.Rect.String = doc.CropBox.String;
                doc.Rendering.Save("shuttle_p" + i + ".png");
            }
        }
    }
}
