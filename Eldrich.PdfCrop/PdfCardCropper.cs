﻿using System.Collections.Generic;
using Eldrich.Common;
using WebSupergoo.ABCpdf10;

namespace Eldrich.PdfCrop
{
    public class PdfCardCropper
    {
        public int Dpi { get; set; }

        public PdfCardCropper(int dpi)
        {
            Dpi = dpi;
        }

        public void Crop(string file, string outFolder, List<ParsedCard> cards)
        {
            //todo: implement
            var doc = new Doc();
            doc.Read(file);

            doc.Rendering.DotsPerInch = Dpi;

            for (int i = 1; i <= 2; i++)
            {
                doc.PageNumber = i;
                doc.Rect.String = doc.CropBox.String;
                doc.Rendering.Save("page" + i + ".png");
            }
        } 
    }
}